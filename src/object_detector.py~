#!/usr/bin/env python

import rospy, cv2, cv_bridge
import imutils
from imutils.object_detection import non_max_suppression
import numpy as np
import datetime

from sensor_msgs.msg import Image

class Detecter:
    def __init__(self, win_stride=(4,4), padding=(8, 8), scale=1.05, mean_shift=False):
        self.bridge = cv_bridge.CvBridge()
        self.hog = cv2.HOGDescriptor()
        self.hog.setSVMDetector(cv2.HOGDescriptor_getDefaultPeopleDetector())
        self.win_stride = win_stride
        self.padding = padding
        self.scale = scale
        self.mean_shift = mean_shift
        
        self.image_sub = rospy.Subscriber('camera/image_raw', Image, self.image_callback)
    def image_callback(self, msg):
        image = self.bridge.imgmsg_to_cv2(msg)
        image = imutils.resize(image, width=min(400, image.shape[1]))
        (rects, weights) = self.hog.detectMultiScale(image,
                                               winStride=self.win_stride,
                                               padding=self.padding,
                                               scale=self.scale,
                                               useMeanshiftGrouping=self.mean_shift)


        rects = np.array([[x, y, x+w, y+h] for (x,y,w,h) in rects])
        pick = non_max_suppression(rects, probs=None, overlapThresh=0.65)

        for(xA, yA, xB, yB) in pick:
            cv2.rectangle(image, (xA, yA), (xB, yB), (0, 255, 0), 2)

        cv2.imshow("NMS", image)
        cv2.waitKey(1)

rospy.init_node('detector')
detecter = Detecter()
rospy.spin()
