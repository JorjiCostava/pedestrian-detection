#!/usr/bin/env python

import rospy, cv2, cv_bridge
import imutils
from imutils.object_detection import non_max_suppression
import numpy as np
import datetime
import sys
import os, rospkg
import random
from sensor_msgs.msg import Image, RegionOfInterest
from detect.srv import Boxes, BoxesResponse
import time

class Detecter:
    def __init__(self, win_stride=(4,4), padding=(8, 8), scale=1.05, mean_shift=False):

        self.bridge = cv_bridge.CvBridge()

        rp = rospkg.RosPack()
        script_path = os.path.join(rp.get_path("detect"), "src")

        mask_rcnn = "rcnn-coco"
        labelsPath = os.path.sep.join([script_path, mask_rcnn, "classes.txt"])
        # Load labels for classes in COCO dataset
        self.LABELS = open(labelsPath).read().strip().split("\n")

        # Load primary and secondary colours for bounding boxes
        coloursPath = os.path.sep.join([script_path , mask_rcnn, "colours.txt"])
        COLOURS = open(coloursPath).read().strip().split("\n")
        COLOURS = [np.array(c.split(",")).astype("int") for c in COLOURS]
        self.COLOURS = np.array(COLOURS, dtype="uint8")

        # Load model weights and config
        weightsPath = os.path.sep.join([script_path, mask_rcnn, "frozen_inference_graph.pb"])
        configPath = os.path.sep.join([script_path, mask_rcnn, "rcnn_coco.pbtxt"])

        self.visualise = 0
        self.confidence = 0.5
        self.threshold = 0.3

        print("[INFO] Loading Mask R-CNN from disk...")

        self.net = cv2.dnn.readNetFromTensorflow(weightsPath, configPath)

        print("[INFO] Neural net loaded. Subscribing to image feed.")

        if rospy.get_param("/display", False):
            # Visual display mode
            self.image_sub = rospy.Subscriber('camera/image_raw',
                                              Image,
                                              self.display_image_cb,
                                              queue_size = 1)
        if rospy.get_param("/compare", False):
            # Comparison mode
            self.service = rospy.Service('boxes',
                                         Boxes,
                                         self.compute_boxes_cb)
        if rospy.get_param("/publish", False):
            raise NotImplementedError()

    def compute_boxes_cb(self, request):
        image = self.bridge.imgmsg_to_cv2(request.image)
        (H, W) = image.shape[:2]
        print("[INFO] Received image of size "+str(W) +","+ str(H))
        blob = cv2.dnn.blobFromImage(image, swapRB=True, crop=False)
        self.net.setInput(blob)
        (boxes, masks) = self.net.forward(["detection_out_final", "detection_masks"])

        #clone = image.copy()

        ret = []
        for i in range(0, boxes.shape[2]):
            classID = int(boxes[0, 0, i, 1])
            confidence = boxes[0, 0, i, 2]

            # If confidence of model is high enough add bounding box
            if confidence > self.confidence:

                #print("[INFO] confidence: "+ str(confidence))

                box = boxes[0, 0, i, 3:7] * np.array([W, H, W, H])
                (startX, startY, endX, endY) = box.astype("int")
                # Convert vertices to region of interest
                boxW = abs(endX - startX)
                boxH = abs(endY - startY)
                roi = RegionOfInterest()
                roi.x_offset = min(startX, endX)
                roi.y_offset = min(startY, endY)
                roi.width = boxW
                roi.height = boxH
                roi.do_rectify = False
                ret.append(roi)
        print("Sending boxes " + str(ret))
        return BoxesResponse(boxes=ret)
        
    def display_image_cb(self, msg):
        """
        Function as a compute node for annotating received messages
        """
        image = self.bridge.imgmsg_to_cv2(msg)
        (H, W) = image.shape[:2]
        blob = cv2.dnn.blobFromImage(image, swapRB=True, crop=False)
        self.net.setInput(blob)
        (boxes, masks) = self.net.forward(["detection_out_final", "detection_masks"])

        clone = image.copy()

        for i in range(0, boxes.shape[2]):
            classID = int(boxes[0, 0, i, 1])
            confidence = boxes[0, 0, i, 2]

            if confidence > self.confidence:

                box = boxes[0, 0, i, 3:7] * np.array([W, H, W, H])
                (startX, startY, endX, endY) = box.astype("int")
                boxW = endX - startX
                boxH = endY - startY

                colour = random.choice(self.COLOURS)

                colour = [int(c) for c in colour]
                cv2.rectangle(clone, (startX, startY), (endX, endY), colour, 2)

                text = "{}: {:.4f}".format(self.LABELS[classID], confidence)
                cv2.putText(clone, text, (startX, startY - 5),
                            cv2.FONT_HERSHEY_SIMPLEX, 0.5, colour, 2)

        cv2.imshow("Output", clone)
        cv2.waitKey(1)

rospy.init_node('detector')

detecter = Detecter()
rospy.spin()
