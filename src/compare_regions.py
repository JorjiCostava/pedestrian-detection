#!/usr/bin/env python

import rospy, cv2, cv_bridge
import imutils
import numpy as np
import sys
import os, rospkg
import re
import itertools
import tabulate

from sensor_msgs.msg import Image, RegionOfInterest
from detect.srv import Boxes

def boxes_to_str(boxes):
    return [str(x) for x in boxes]

def boxes_accuracy(pair):
    # Find accuracy of a pair of lists of bounding boxes
    acc = 0
    firstLen = len(pair[0])
    lastLen = len(pair[1])
    maxLen = max(firstLen, lastLen)
    minLen = min(firstLen, lastLen)
    for i in range(maxLen):
        if i < minLen:
            acc += (pair[0][i]).accuracy(pair[1][i])
    return acc / maxLen

def intersection_over_union(boxesA, boxesB):
    # Find all permutations of both lists
    permA = list(itertools.permutations(boxesA,len(boxesA)))
    permB = list(itertools.permutations(boxesB,len(boxesB)))
    pairs = list(itertools.product(permA, permB))
    # Find cartesian product with maximum accuracy
    pair = max(pairs, key=boxes_accuracy)
    return boxes_accuracy(pair)

def squares_from_msg(msg):
    # Convert ROS message to list of boxes
    ret = []
    for box in msg.boxes:
        x, y, width, height = box.x_offset, box.y_offset, box.width, box.height
        ret.append(Square(x, y, x+width, y+height))
    return ret
    
class Square:
    """
    Represents a single bounding box
    """
    def __init__(self, xA, yA, xB, yB):
        xA = int(xA)
        xB = int(xB)
        yA = int(yA)
        yB = int(yB)
        if xA < xB:
            self.xA = xA
            self.xB = yB
        else:
            self.xB = xA
            self.xA = xB
        if yA < yB:
            self.yA = yA
            self.yB = yB
        else:
            self.yB = yA
            self.yA = yB
    def width(self):
        return self.xB - self.xA
    def height(self):
        return self.yB - self.yA
    def area(self):
        return self.width() * self.height()
    def interArea(self, other):
        """
        Area of overlap with other bounding box
        """
        width = min(self.xB, other.xB) - max(self.xA, other.xA)
        if width <= 0:
            return 0
        height = min(self.yB, other.yB) - max(self.yA, other.yA)
        if height <= 0:
            return 0
        return width * height
    def unionArea(self, other):
        """
        Area resulting from merging this box with other bounding box
        """
        return self.area() + other.area() - self.interArea(other)
    def accuracy(self, other):
        interArea = self.interArea(other)
        unionArea = self.area() + other.area() - interArea
        return interArea / float(unionArea)
    def draw(self, img, colour):
        cv2.rectangle(img, (self.xA, self.yA), (self.xB, self.yB), colour, 2)
    def __str__(self):
        return "Square(sw=(%s, %s), ne=(%s, %s))"%(self.xA, self.yA, self.xB, self.yB)


class CompareRegions:
    def __init__(self):
        self.bridge = cv_bridge.CvBridge()
        rp = rospkg.RosPack()
        root_path = os.path.join(rp.get_path("detect"), "src", "PennFudanPed")
        # Load image path
        self.image_path = os.path.join(root_path, "PNGImages")
        # Load ground truth bounding box annotations
        self.annotations_path = os.path.join(root_path, "Annotation")
        # Both image filename types
        self.image_fn = ("FudanPed%05d.png", "PennPed%05d.png")
        # Both annotation filename types
        self.annotate_fn = ("FudanPed%05d.txt", "PennPed%05d.txt")
        # Regular expression to match annotation format
        self.box_re = re.compile("\(Xmin, Ymin\) - \(Xmax, Ymax\) : \((?P<xMax>\d+), (?P<yMax>\d+)\) - \((?P<xMin>\d+), (?P<yMin>\d+)\)")
        self.counter = 24
        # Wait for service nodes
        rospy.wait_for_service("hog_boxes")
        rospy.wait_for_service("frcnn_boxes")
        self.hogsvm_boxes = rospy.ServiceProxy("hog_boxes", Boxes)
        self.frcnn_boxes = rospy.ServiceProxy("frcnn_boxes", Boxes)
        self.image_pub = rospy.Publisher("camera/image_raw", Image, queue_size=1)
        self.total_accuracy_hog = 0
        self.hog_count = 0
        self.total_accuracy_cnn = 0
        self.cnn_count = 0
    def get_counter_name(self):
        """
        Alternate between images from Fudan and Penn when testing
        """
        counter = self.counter + 1
        if counter & 1:
            image_fn = self.image_fn[0]
        else:
            image_fn = self.image_fn[1]
        counter >>= 1
        return image_fn % (counter,)
    def get_annotation_fn(self):
        """
        Alternate between annotations from Fudan and Penn when testing
        """
        counter = self.counter + 1
        if counter & 1:
            annotate_fn = self.annotate_fn[0]
        else:
            annotate_fn = self.annotate_fn[1]
        counter >>= 1
        return os.path.join(self.annotations_path, annotate_fn % (counter,))
    def real_squares(self):
        # Ground truth squares
        annotation = open(self.get_annotation_fn())
        squares = []
        for line in annotation.readlines():
            match = self.box_re.search(line)
            if match:
                    squares.append(Square(match.group("xMin"), match.group("yMin"), match.group("xMax"), match.group("yMax")))
        return squares
    def get_image_fn(self):
        """
        Alternate between images from Fudan and Penn when testing
        """
        counter = self.counter + 1
        if counter & 1:
            image_fn = self.image_fn[0]
        else:
            image_fn = self.image_fn[1]
        counter >>= 1
        return os.path.join(self.image_path, image_fn % (counter,))
    def get_image(self):
        return cv2.imread(self.get_image_fn())
    def get_image_msg(self):
        # Resize image and convert it to ROS message
        img = self.get_image()
        img = imutils.resize(img, width=min(400, img.shape[1]))
        return self.bridge.cv2_to_imgmsg(img)
    def send_image(self):
        # Publish image
        self.image_pub.publish(self.get_image_msg())
    def compare_boxes(self):
        # Compare accuracy
        img = self.get_image_msg()
        hog_boxes = squares_from_msg(self.hogsvm_boxes(img))
        print("hog_boxes = %s" %(boxes_to_str(hog_boxes),))
        frcnn_boxes = squares_from_msg(self.frcnn_boxes(img))
        print("frcnn_boxes = %s" %(boxes_to_str(frcnn_boxes),))
        real_boxes = self.real_squares()
        print("real_boxes = %s" %(boxes_to_str(real_boxes),))
        hog_accuracy = intersection_over_union(hog_boxes, real_boxes)
        self.total_accuracy_hog += hog_accuracy
        self.hog_count += 1
        frcnn_accuracy = intersection_over_union(frcnn_boxes, real_boxes)
        self.total_accuracy_cnn += frcnn_accuracy
        self.cnn_count += 1 
        print("Accuracy of hog = %s" % (hog_accuracy,))
        print("Accuracy of rcnn = %s" % (frcnn_accuracy,))
        print("Average accuracy of hog = %s" % (self.total_accuracy_hog / self.hog_count))
        print("Average accuracy of rcnn = %s" % (self.total_accuracy_cnn / self.cnn_count))
        # Draw bounding boxes of ground truth, HOG and CNN
        img = self.get_image().copy()
        height, width, _ = img.shape
        green = (0,255,0)
        for box in real_boxes:
            box.draw(img, green)
        blue = (255,0,0)
        for box in frcnn_boxes:
            box.draw(img, blue)
        red = (0,0,255)
        for box in hog_boxes:
            box.draw(img, red)
        cv2.putText(img, "Truth=%1.2f".rjust(6)%(1.0,), (0,height-60),
                    cv2.FONT_HERSHEY_SIMPLEX, 0.5, green, 2)
        cv2.putText(img, "HOG=%1.2f".rjust(6)%(hog_accuracy,), (0,height-40),
                    cv2.FONT_HERSHEY_SIMPLEX, 0.5, red, 2)
        cv2.putText(img, "FRCNN=%1.2f".rjust(6)%(frcnn_accuracy,), (0,height-20),
                    cv2.FONT_HERSHEY_SIMPLEX, 0.5, blue, 2)

        orange = (0, 255, 255)
        cv2.putText(img, "File=%s"%(self.get_counter_name(),), (0,20),
                    cv2.FONT_HERSHEY_SIMPLEX, 0.5, orange, 2)
        cv2.imshow("Comparison", img)
        cv2.waitKey(0)

        #self.accuracy_records.append(hog_accuracy, rcnn_accuracy)

    def next_image(self):
        self.counter += 1

rospy.init_node('compare')
detector = CompareRegions()

while not rospy.is_shutdown():
    detector.compare_boxes()
    #detector.send_image()
    detector.next_image()
