#!/usr/bin/env python

import rospy, cv2, cv_bridge
import imutils
from imutils.object_detection import non_max_suppression
import numpy as np
import datetime
import sys
import os, rospkg
import sys

from sensor_msgs.msg import Image

class Streamer:
    def __init__(self):
        self.bridge = cv_bridge.CvBridge()
        self.image_pub = rospy.Publisher("camera/image_raw", Image, queue_size=1)
        if len(sys.argv) < 2:
            print("You must give an argument to open a video stream")
            exit(0)

        resource = sys.argv[1]

        if len(resource) < 3:
            resource_name = "/dev/video" + resource
            resource = int(resource)
        else:
            resource_name = resource

        cap = cv2.VideoCapture(resource)

        if not cap.isOpened():
            print("Error opening resource: "+ str(resource))
            exit(0)

        self.rate = rospy.Rate(15)

        rval, frame = cap.read()
        while rval and not rospy.is_shutdown():
            #frame = imutils.resize(frame, width=min(400, frame.shape[1]))
            image = self.bridge.cv2_to_imgmsg(frame)
            self.image_pub.publish(image)
            rval, frame = cap.read()
            self.rate.sleep()

rospy.init_node('video_stream')

streamer = Streamer()
