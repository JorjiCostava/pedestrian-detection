#!/usr/bin/env python

import rospy, cv2, cv_bridge
import imutils
from imutils.object_detection import non_max_suppression
import numpy as np
import datetime
import sys


from sensor_msgs.msg import Image, RegionOfInterest
from detect.srv import Boxes, BoxesResponse

class Detecter:
    def __init__(self, win_stride=(2,2), padding=(8, 8), scale=1.05, mean_shift=False):
        self.bridge = cv_bridge.CvBridge()
        self.hog = cv2.HOGDescriptor()
        # Load SVM model
        self.hog.setSVMDetector(cv2.HOGDescriptor_getDefaultPeopleDetector())
        self.win_stride = win_stride
        self.padding = padding
        self.scale = scale
        self.mean_shift = mean_shift
        if rospy.get_param("/display", False):
            # Display mode
            self.image_sub = rospy.Subscriber('camera/image_raw',
                                              Image,
                                              self.display_image_cb,
                                              queue_size = 1)
        if rospy.get_param("/compare", False):
            # Compare mode
            self.service = rospy.Service('boxes',
                                         Boxes,
                                         self.compute_boxes_cb)
        if rospy.get_param("/publish", False):
            raise NotImplementedError()
    def display_image_cb(self, msg):
        # Display image message received to the user annotated with bounding boxes
        image = self.bridge.imgmsg_to_cv2(msg)
        image = imutils.resize(image, width=min(400, image.shape[1]))
        (rects, weights) = self.hog.detectMultiScale(image,
                                               winStride=self.win_stride,
                                               padding=self.padding,
                                               scale=self.scale,
                                               useMeanshiftGrouping=self.mean_shift)


        rects = np.array([[x, y, x+w, y+h] for (x,y,w,h) in rects])
        pick = non_max_suppression(rects, probs=None, overlapThresh=0.65)

        for(xA, yA, xB, yB) in pick:
            cv2.rectangle(image, (xA, yA), (xB, yB), (0, 255, 0), 2)

        cv2.imshow("NMS", image)
        cv2.waitKey(1)

    def compute_boxes_cb(self, request):
        # Marshall bounding boxes and send to client proxy
        image = self.bridge.imgmsg_to_cv2(request.image)
        oldHeight, oldWidth, _ = image.shape
        #image = imutils.resize(image, width=min(400, image.shape[1]))
        newHeight, newWidth, _ = image.shape
        (rects, weights) = self.hog.detectMultiScale(image,
                                                     winStride=self.win_stride,
                                                     padding=self.padding,
                                                     scale=self.scale,
                                                     useMeanshiftGrouping=self.mean_shift)


        rects = np.array([[x, y, x+w, y+h] for (x,y,w,h) in rects])
        pick = non_max_suppression(rects, probs=None, overlapThresh=0.65)

        ret = []

        widthRatio = oldWidth / float(newWidth)
        heightRatio = oldHeight / float(newHeight)

        for(xA, yA, xB, yB) in pick:
            roi = RegionOfInterest()
            roi.x_offset = min(xA, xB) # * widthRatio
            roi.y_offset = min(yA, yB) #* heightRatio
            roi.width = abs(xB - xA) #* widthRatio
            roi.height = abs(yB - yA) #* heightRatio
            roi.do_rectify = False
            ret.append(roi)

        return BoxesResponse(boxes=ret)

rospy.init_node('detector')
detecter = Detecter()
rospy.spin()
